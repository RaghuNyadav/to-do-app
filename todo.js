// $myBody = document.querySelector('body')

$inputValue = document.querySelector('#todo')
$todoDiv = document.querySelector('.todoList')
$list = document.createElement('ol')
$todoDiv.append($list)
$button = document.querySelector('button')

let liData = []


// let count = 0;
const addToDo = (evt) => {
    // console.log(evt.target)
    // console.log(evt.target.parentElement.clientHeight)
    // console.log(event.keyCode)
    const num = evt.target.parentElement.clientHeight
    // const keyName = 'a'
    // count++
    $inputValue.focus()
    if ($inputValue.value) {
        // alert($inputValue.value)
        $liItem = document.createElement('li')

        $liItem.innerHTML = `${$inputValue.value}<input type='checkbox'> <button class='inside_button'>Delete</button><button id="edit">Edit</button>`

        liData.push($inputValue.value)
        localStorage.setItem(`${num}`, $liItem.innerHTML)

        // if (localStorage.getItem('data') === null) {
        //     localStorage.setItem('data', JSON.stringify(liData))
        // } else {
        //     liData += localStorage.getItem('data')
        //     localStorage.setItem('data', JSON.stringify(liData))
        // }

        // console.log(localStorage)
        $list.append($liItem)
        $inputValue.value = ''
    }

}
$button.addEventListener('click', addToDo)
const onEnterKey = (event) => {
    // console.log(event.target)

    // const keyNameOne = 'b'
    num = event.target.parentElement.clientHeight

    // count++
    $inputValue.focus()
    if (event.keyCode === 13 && $inputValue.value) {
        $liItem = document.createElement('li')

        $liItem.innerHTML = `${$inputValue.value} <input type='checkbox'> <button class='inside_button'>Delete</button><button id="edit">Edit</button>`
        localStorage.setItem(`${num}`, $liItem.innerHTML)

        // liData.push($inputValue.value)
        // localStorage.setItem(data, JSON.stringify(liData))
        $list.append($liItem)
        $inputValue.value = ''
        // console.log(localStorage)
    }


}

const deleteTodo = (evt) => {
    // evt.preventDefault()
    // console.log(evt.target)
    const attribute = evt.target
    if (attribute.type === 'checkbox' && attribute.checked) {
        // console.log(attribute.parentNode.innerHTML)
        let data1 = attribute.parentNode.innerHTML

        attribute.parentNode.lastChild.disabled = true
        attribute.parentNode.lastChild.innerHTML = 'Task Completed'
        attribute.hidden = true
        // attribute.parentNode.style.backgroundColor = 'green'

        let data2 = attribute.parentNode.innerHTML
        Object.keys(localStorage).forEach(key => {
            if (localStorage.getItem(key) === data1) {
                localStorage.setItem(key, data2)
            }
        })
        // console.log(attribute.parentNode.innerHTML)

    }

    // else if (!attribute.checked && attribute.type === 'checkbox') {

    //     attribute.parentNode.lastChild.disabled = false
    //     attribute.parentNode.lastChild.innerHTML = 'Edit'
    //     // attribute.parentNode.style.backgroundColor = ''
    //     // console.log(attribute.parentNode.innerHTML)


    // }
    if (attribute.className === 'inside_button') {
        // console.log(attribute.parentNode.textContent)
        // console.log(Object.values(localStorage))
        let x = attribute.parentNode.innerHTML.trim()
        // console.log(x)
        Object.keys(localStorage).forEach(key => {
            if (localStorage.getItem(key) === x) {
                localStorage.removeItem(key)
            }
        })
        attribute.parentNode.remove()
    }
    if (attribute.id === 'edit') {
        let y = attribute.parentNode.innerHTML.trim()
        // console.log(y)

        const newTodo = prompt('Enter Your new To Do')
        attribute.parentNode.firstChild.textContent = newTodo
        let x = attribute.parentNode.innerHTML.trim()
        Object.keys(localStorage).forEach(key => {
            if (localStorage.getItem(key) === y) {
                localStorage.setItem(key, x)
            }
        })
        // console.log(x)
    }
}

$list.addEventListener('click', deleteTodo)

window.reload = restore()

function restore() {
    // console.log(Object.values(localStorage))
    // console.log(localStorage.getItem('data'))

    Object.keys(localStorage).map(key => {
        $liItem = document.createElement('li')
        // myStorage.setItem(`${keyName + count}`, `${$inputValue.value}`)
        $liItem.innerHTML = `${localStorage.getItem(key)}`
        $list.append($liItem)
        // $inputValue.value = ''
    })
}

// localStorage.clear()